#language: pt
#encoding: ISO-8859-1
Funcionalidade: Preencher formulario Tricents

  Esquema do Cenario: Cadastro preenchido com sucesso
    Dado que estou no site "http://sampleapp.tricentis.com/101/app.php"
    E preencho o formulario "Enter Vehicle Data"
    E pressiono next
    E preencho o formulario da aba "Enter Insurant Data"
    E pressiono next
    E preencho os campos do formulario da aba "Enter Product Data"
    E pressiono next
    E seleciono uma opcao de "Select Price Option"
    E pressiono next
    Quando preencher o formulario "Send Quote"
    E clico em enviar
    Entao devo receber a mensagem em um modal "Sending e-mail success!"
