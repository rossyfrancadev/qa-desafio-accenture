# Desafio Accenture
![](src/test/resources/assets/7wqs3X4t_400x400.jpg)
```
1 Criar projeto em selenium(java) e cucumber, utilizando o padrão Page objects.
```
```
2 Executar os passos:
*	Entrar no site http://sampleapp.tricentis.com/101/app.php
*	Preencher o formulário, aba “Enter Vehicle Data” e pressione next
*	Preencher o formulário, aba “Enter Insurant Data” e pressione next
*	Preencher o formulário, aba “Enter Product Data” e pressione next
*	Preencher o formulário, aba “Select Price Option” e pressione next
*	Preencher o formulário, aba “Send Quote” e pressione Send
*	Verificar a mensagem “Sending e-mail success!” na tela
```


## Ferramentas e tecnologias utilizadas:

IDE: Eclipse
Linguagem: Java
Frameworks:
- [Maven](https://maven.apache.org/) 
> Gerenciador de dependências
- [Junit](https://junit.org/junit4/)
> Asserção e casos de testes
- [Cucumber](https://cucumber.io/) 
> Estrutura para BDD
- [Selenium Web driver](https://www.selenium.dev/)
> Motor chrome para automatização
- [Poi](https://poi.apache.org/)
> Gerador de arquivos xdoc

 O projeto foi configurado para ser executado logo após ser clonado, sem dependências externas.

### Gerador de evidências

Foi acrescentado um arquivo que criei na minha época de estágio, no qual ele gera no final do ciclo de teste documentos .xdoc contendo os prints e algumas referências do que foin executado.
Esse arquivo eu já tinha ele pronto, foi somente o trabalho de implementar.
Por default ele irá gravar as evidências em C:\evidencias.

### Utilização do projeto (importante!)
- Deve ser realizado o download do arquivo .zip
![](src/test/resources/assets/download_zip.PNG)

- descompactar > importar como projeto maven
- Para rodar como proposto pelo desafio, abrir o arquivo src/test/resources/features/TricentisForm.frature  > clicar com o botão direito do mouse e selecionar run as > cucumber feature, que o o fluxo completo que foi delineado será executado. 

### Agradecimento

Fico desde já agradecido pela oportunidade de tentar esta vaga para uma empresa do porte da Accenture, líder no mercado de consultoria em ti e transformação digital como também aprimorar as minhas habilidades.

```
May the force be with us.
```
