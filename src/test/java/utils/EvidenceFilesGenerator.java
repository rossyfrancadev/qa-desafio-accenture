package utils;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.ArrayList;

import org.apache.poi.openxml4j.exceptions.InvalidFormatException;
import org.apache.poi.util.Units;
import org.apache.poi.xwpf.model.XWPFHeaderFooterPolicy;
import org.apache.poi.xwpf.usermodel.ParagraphAlignment;
import org.apache.poi.xwpf.usermodel.XWPFDocument;
import org.apache.poi.xwpf.usermodel.XWPFParagraph;
import org.apache.poi.xwpf.usermodel.XWPFRun;
import org.openxmlformats.schemas.wordprocessingml.x2006.main.CTP;
import org.openxmlformats.schemas.wordprocessingml.x2006.main.CTR;
import org.openxmlformats.schemas.wordprocessingml.x2006.main.CTSectPr;
import org.openxmlformats.schemas.wordprocessingml.x2006.main.CTText;

import static utils.Configurations.*;

public class EvidenceFilesGenerator {

	public static void createEvidenceDoc(String test, ArrayList<String> paragrafos) throws InvalidFormatException {
		if (gerarEvidencias) {
			// Cabeçalho do arquivo
			XWPFDocument docx = new XWPFDocument();
			CTSectPr sectPr = docx.getDocument().getBody().addNewSectPr();
			XWPFHeaderFooterPolicy policy = new XWPFHeaderFooterPolicy(docx, sectPr);
			// Header
			CTP ctpHeader = CTP.Factory.newInstance();
			CTR ctrHeader = ctpHeader.addNewR();
			CTText ctHeader = ctrHeader.addNewT();
			String headerText = "DOCUMENTO DE EVIDÊNCIAS";
			ctHeader.setStringValue(headerText);
			XWPFParagraph headerParagraph = new XWPFParagraph(ctpHeader, docx);
			headerParagraph.setAlignment(ParagraphAlignment.CENTER);
			XWPFParagraph[] parsHeader = new XWPFParagraph[1];
			parsHeader[0] = headerParagraph;
			policy.createHeader(XWPFHeaderFooterPolicy.DEFAULT, parsHeader);

			// Rodapé do arquivo
			CTP ctpFooter = CTP.Factory.newInstance();
			CTR ctrFooter = ctpFooter.addNewR();
			CTText ctFooter = ctrFooter.addNewT();
			String footerText = "Gerador de arquivos de evidências - by França";
			ctFooter.setStringValue(footerText);
			XWPFParagraph footerParagraph = new XWPFParagraph(ctpFooter, docx);
			footerParagraph.setAlignment(ParagraphAlignment.CENTER);
			XWPFParagraph[] parsFooter = new XWPFParagraph[1];
			parsFooter[0] = footerParagraph;
			policy.createFooter(XWPFHeaderFooterPolicy.DEFAULT, parsFooter);

			// Título
			XWPFParagraph titulo = docx.createParagraph();
			titulo.setAlignment(ParagraphAlignment.CENTER);
			XWPFRun run = titulo.createRun();
			run.setBold(true);
			run.setFontSize(15);
			run.setText("Evidências de testes: " + test);

			// Corpo do arquivo
			for (int i = 0; i < paragrafos.size(); i++) {

				XWPFParagraph bodyParagraph = docx.createParagraph();
				bodyParagraph.setAlignment(ParagraphAlignment.CENTER);
				XWPFRun r = bodyParagraph.createRun();
				r.setBold(true);
				r.setText("Teste executado: " + paragrafos.get(i));

				String imgFile = "C:\\evidencias\\screenshots\\" + paragrafos.get(i) + ".png";

				try {
					r.addPicture(new FileInputStream(imgFile), XWPFDocument.PICTURE_TYPE_PNG, imgFile, Units.toEMU(500),
							Units.toEMU(300));
				} catch (IOException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}

			}

			FileOutputStream out = null;
			try {
				out = new FileOutputStream("C:\\evidencias\\" + test + ".docx");
			} catch (FileNotFoundException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			try {
				docx.write(out);
				out.close();
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}

			System.out.println("criado documento de evidências");
		} else {
			return;
		}
	}

}
