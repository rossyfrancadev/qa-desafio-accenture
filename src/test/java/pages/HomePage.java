package pages;

import org.openqa.selenium.WebDriver;

public class HomePage extends DefaultPage {

	public HomePage(WebDriver driver) {

		super(driver);

	}

	public String nextButton = "button.next";
	public String toastError = "div.field.idealforms-field.idealforms-field-text.invalid > span.error";
	public String sendQuoteToastError = "section#sendQuoteForm > div > span.error";
	public String usernameToastError = "div.field.idealforms-field.idealforms-field-text.invalid > span.error";
	public String passwordToastError = "div.field.idealforms-field.idealforms-field-password.invalid > span.error";
	public String confirmPasswordToastError = "div.field.idealforms-field.idealforms-field-password.invalid > span.error";
	// Enter Vehicle Data Form
	public String dropDownMake = "select#make";
	public String dropDownModel = "select#model";
	public String cilinderCapacity = "input#cylindercapacity";
	public String enginerPerformance = "input#engineperformance";
	public String dateOfManufacture = "input#dateofmanufacture";
	public String numberOfSeats = "select#numberofseats";
	public String rightHandDriver = "input#righthanddriveyes";
	public String numberOfSeatsMotorCycle = "select#numberofseatsmotorcycle";
	public String fuelType = "select#fuel";
	public String payload = "input#payload";
	public String totalWeight = "input#totalweight";
	public String listPrice = "input#listprice";
	public String annualMileage = "input#annualmileage";
	// Enter Insurant Data Form
	public String firstName = "input#firstname";
	public String lastName = "input#lastname";
	public String birthDate = "input#birthdate";
	public String country = "select#country";
	public String zipCode = "input#zipcode";
	public String occupation = "select#occupation";
	public String hobbieSpeeding = "label.ideal-radiocheck-label > input#speeding";
	// Enter Product Data Form
	public String startDate = "input#startdate";
	public String insuranceSum = "select#insurancesum";
	public String meritRating = "select#meritrating";
	public String damageinsurance = "select#damageinsurance";
	public String euroProtection = "input#EuroProtection";
	public String courtesyCar = "select#courtesycar";
	// Select Price Option
	public String selectUltimate = "input#selectultimate";
	// Send Quote Form
	public String email = "input#email";
	public String username = "input#username";
	public String password = "input#password";
	public String confirmPassword = "input#confirmpassword";
	public String sendMail = "button#sendemail";

}
