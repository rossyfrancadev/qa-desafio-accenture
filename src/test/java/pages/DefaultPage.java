package pages;

import static utils.Configurations.*;

import java.io.File;
import java.util.List;

import org.apache.commons.io.FileUtils;
import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.OutputType;
import org.openqa.selenium.TakesScreenshot;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.Select;
import org.openqa.selenium.support.ui.WebDriverWait;

public class DefaultPage {

	WebDriver driver;

	public DefaultPage(WebDriver driver) {
		this.driver = driver;
	}

	public DefaultPage() {

		ChromeOptions options = new ChromeOptions();

		// utlis.Configuration para setar variável que determina se o chrome irá rodar
		// visualmente ou não
		if (headless)
			options.addArguments("--headless");

		this.driver = new ChromeDriver(options);

	}

	public WebDriver getDriver() {

		return driver;

	}

	public void navigateTo(String url) {

		driver.navigate().to(url);
		driver.manage().window().maximize();

	}

	public void closeNavigator() {

		getDriver().close();

	}

	public WebElement getElement(String cssSelector) {

		WebElement element = getDriver().findElement(By.cssSelector(cssSelector));
		return element;

	}

	public String getPageTitle() {

		return getDriver().getTitle();

	}

	public String getCurrentUrl() {

		return getDriver().getCurrentUrl();

	}

	public void selectItemDropDown(String cssSelector, int index) {

		Select dropDown = new Select(getDriver().findElement(By.cssSelector(cssSelector)));
		dropDown.selectByIndex(index);

	}

	public void selectItemDropDownByText(String cssSelector, String text) {

		Select dropDown = new Select(getDriver().findElement(By.cssSelector(cssSelector)));
		dropDown.deselectByVisibleText(text);

	}

	public void inputText(String cssSelector, String value) {

		WebElement element = getDriver().findElement(By.cssSelector(cssSelector));
		element.clear();
		element.sendKeys(value);

	}

	public void selectCheckBox(String cssSelector) {

		getDriver().findElement(By.cssSelector(cssSelector)).click();

	}

	public void clickButton(String cssSelector) {

		getDriver().findElement(By.cssSelector(cssSelector)).click();

	}

	public List<WebElement> getListElements(String cssSelector) {

		List<WebElement> list = getDriver().findElements(By.cssSelector(cssSelector));
		return list;

	}

	public void selectOneFromCheckBoxList(String cssSelector, int index) {

		List<WebElement> list = getListElements(cssSelector);
		list.get(index).click();

	}

	public void javaScript(String script) {
		JavascriptExecutor js = (JavascriptExecutor) driver;
		js.executeScript(script);
	}

	public void waitToElement(String cssSelector) {

		WebDriverWait wait = new WebDriverWait(getDriver(), 100);
		wait.until(ExpectedConditions.visibilityOfAllElementsLocatedBy(By.cssSelector(cssSelector)));

	}

	public File screeshot(String photoName) throws Exception {
		if (gerarEvidencias) {
			File file = ((TakesScreenshot) this.getDriver()).getScreenshotAs(OutputType.FILE);
			File destFile = new File("C:\\evidencias\\screenshots\\" + photoName + ".png");
			FileUtils.copyFile(file, destFile);
			System.out.println("screenshot:" + photoName);
			return file;
		} else {
			return null;
		}
	}

}
