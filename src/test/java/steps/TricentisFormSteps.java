package steps;

import static org.junit.Assert.assertEquals;
import static utils.Configurations.url_base;

import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;

import org.apache.poi.openxml4j.exceptions.InvalidFormatException;
import org.openqa.selenium.WebElement;

import io.cucumber.java.After;
import io.cucumber.java.Before;
import io.cucumber.java.pt.Dado;
import io.cucumber.java.pt.E;
import io.cucumber.java.pt.Entao;
import io.cucumber.java.pt.Quando;
import pages.DefaultPage;
import pages.HomePage;
import utils.EvidenceFilesGenerator;

public class TricentisFormSteps {

	protected DefaultPage page = new DefaultPage();
	protected HomePage home;
	static ArrayList<String> evidencias = new ArrayList<String>();
	static String titulo = "Preencher formulario Tricents";

	@Before
	public void before() {
		home = new HomePage(this.page.getDriver());
		home.navigateTo(url_base);
	}

	@After
	public void after() throws InvalidFormatException {
		
		home.closeNavigator();
	}

	@Dado("que estou no site {string}")
	public void que_estou_no_site(String url) {

		String currentUrl = home.getCurrentUrl();
		assertEquals(currentUrl, url);

	}

	@E("preencho o formulario {string}")
	public void preencho_o_formulario(String pageTitle) throws Exception {

		String formulario = home.getPageTitle();
		assertEquals(formulario, pageTitle);

		home.selectItemDropDown(home.dropDownMake, 2);
		home.selectItemDropDown(home.dropDownModel, 1);

		// Valida que o valor mínimo aceito neste campo é 1 e o maior 2000
		home.inputText(home.cilinderCapacity, "0");
		WebElement textoDeErro = home.getElement(home.toastError);
		assertEquals(textoDeErro.getAttribute("innerText"), "Must be a number between 1 and 2000");
		home.inputText(home.cilinderCapacity, "2001");
		assertEquals(textoDeErro.getAttribute("innerText"), "Must be a number between 1 and 2000");
		home.inputText(home.cilinderCapacity, "2000");
		// Valida que o valor mínimo aceito neste campo é 1 e o maior 2000
		home.inputText(home.enginerPerformance, "0");
		assertEquals(textoDeErro.getAttribute("innerText"), "Must be a number between 1 and 2000");
		home.inputText(home.enginerPerformance, "2001");
		assertEquals(textoDeErro.getAttribute("innerText"), "Must be a number between 1 and 2000");
		home.inputText(home.enginerPerformance, "2000");

		home.inputText(home.dateOfManufacture, "07/08/1985");
		home.selectItemDropDown(home.numberOfSeats, 2);
		home.selectItemDropDown(home.numberOfSeatsMotorCycle, 2);
		home.selectItemDropDown(home.fuelType, 3);

		// Valida que o valor mínimo aceito neste campo é 1 e o maior 1000
		home.inputText(home.payload, "0");
		textoDeErro = home.getElement(home.toastError);
		assertEquals(textoDeErro.getAttribute("innerText"), "Must be a number between 1 and 1000");
		home.inputText(home.payload, "1001");
		assertEquals(textoDeErro.getAttribute("innerText"), "Must be a number between 1 and 1000");
		home.inputText(home.payload, "1000");

		// Valida que o valor mínimo aceito neste campo é 100 e o maior 50000
		home.inputText(home.totalWeight, "99");
		textoDeErro = home.getElement(home.toastError);
		assertEquals(textoDeErro.getAttribute("innerText"), "Must be a number between 100 and 50000");
		home.inputText(home.totalWeight, "50001");
		assertEquals(textoDeErro.getAttribute("innerText"), "Must be a number between 100 and 50000");
		home.inputText(home.totalWeight, "50000");

		// Valida que o valor mínimo aceito neste campo é 500 e o maior 100000
		home.inputText(home.listPrice, "499");
		textoDeErro = home.getElement(home.toastError);
		assertEquals(textoDeErro.getAttribute("innerText"), "Must be a number between 500 and 100000");
		home.inputText(home.listPrice, "100001");
		assertEquals(textoDeErro.getAttribute("innerText"), "Must be a number between 500 and 100000");
		home.inputText(home.listPrice, "100000");

		// Valida que o valor mínimo aceito neste campo é 100 e o maior 100000
		home.inputText(home.annualMileage, "99");
		textoDeErro = home.getElement(home.toastError);
		assertEquals(textoDeErro.getAttribute("innerText"), "Must be a number between 100 and 100000");
		home.inputText(home.annualMileage, "100001");
		assertEquals(textoDeErro.getAttribute("innerText"), "Must be a number between 100 and 100000");
		home.inputText(home.annualMileage, "100000");

		evidencias.add("preencho o formulario " + pageTitle);
		home.screeshot("preencho o formulario " + pageTitle);

	}

	@E("pressiono next")
	public void pressiono_next() {
		home.javaScript("document.querySelector('" + home.nextButton + "').click()");
	}

	@E("preencho o formulario da aba {string}")
	public void preencho_o_formulario_da_aba_e_pressiono_next(String pageTitle) throws Exception {

		String formulario = home.getPageTitle();
		assertEquals(formulario, pageTitle);

		home.inputText(home.firstName, "Rossywan");
		home.inputText(home.lastName, "Franca");
		home.inputText(home.birthDate, "07/08/1985");
		home.selectItemDropDown(home.country, 20);

		// Valida que o número do zipcode tem entre 4 e 8 dígitos
		home.inputText(home.zipCode, "111");
		WebElement textoDeErro = home.getElement(home.toastError);
		assertEquals(textoDeErro.getAttribute("innerText"), "Must be a number between 4 and 8 digits");
		home.inputText(home.zipCode, "123456789");
		textoDeErro = home.getElement(home.toastError);
		assertEquals(textoDeErro.getAttribute("innerText"), "Must be a number between 4 and 8 digits");
		home.inputText(home.zipCode, "12345678");

		home.selectItemDropDown(home.occupation, 1);
		home.javaScript("return document.querySelector('input#speeding').click()");

		evidencias.add("preencho o formulario " + pageTitle);
		home.screeshot("preencho o formulario " + pageTitle);
	}

	@Dado("preencho os campos do formulario da aba {string}")
	public void preencho_os_campos_do_formulario_da_aba(String pageTitle) throws Exception {

		String formulario = home.getPageTitle();
		assertEquals(formulario, pageTitle);

		// Valida que o campo start date tem que ser pelo menos 1 mês no futuro
		LocalDate agora = LocalDate.now().plusMonths(1).minusDays(1);
		DateTimeFormatter formatterData = DateTimeFormatter.ofPattern("MM/dd/YYY");
		String dataFormatada = formatterData.format(agora);
		home.inputText(home.startDate, dataFormatada);
		WebElement textoDeErro = home.getElement(home.toastError);
		assertEquals(textoDeErro.getAttribute("innerText"), "Must be more than one month in the future");

		agora = LocalDate.now().plusMonths(1).plusDays(1);
		formatterData = DateTimeFormatter.ofPattern("MM/dd/YYY");
		dataFormatada = formatterData.format(agora);
		home.inputText(home.startDate, dataFormatada);

		home.selectItemDropDown(home.insuranceSum, 9);
		home.selectItemDropDown(home.meritRating, 1);
		home.selectItemDropDown(home.damageinsurance, 1);
		home.javaScript("document.querySelector('" + home.euroProtection + "').click()");
		home.selectItemDropDown(home.courtesyCar, 1);

		evidencias.add("preencho o formulario " + pageTitle);
		home.screeshot("preencho o formulario " + pageTitle);

	}

	@E("seleciono uma opcao de {string}")
	public void seleciono_uma_opcao_de(String pageTitle) throws Exception {

		String formulario = home.getPageTitle();
		assertEquals(formulario, pageTitle);

		home.javaScript("document.querySelector('" + home.selectUltimate + "').click()");

		evidencias.add("seleciono uma opcao de " + pageTitle);
		home.screeshot("seleciono uma opcao de " + pageTitle);
	}

	@Quando("preencher o formulario {string}")
	public void preencher_o_formulario(String pageTitle) throws Exception {

		String formulario = home.getPageTitle();
		assertEquals(formulario, pageTitle);

		// Valida formato do email
		home.inputText(home.email, "rossyfranca@gmail");
		WebElement textoDeErro = home.getElement(home.sendQuoteToastError);
		assertEquals(textoDeErro.getAttribute("innerText"), "Must be at least a valid email format");
		home.inputText(home.email, "rossyfranca@gmail.com");

		// Valida dados de entrada de 4 a 32 caracteres
		home.inputText(home.username, "ade");
		textoDeErro = home.getElement(home.usernameToastError);
		assertEquals(textoDeErro.getAttribute("innerText"),
				"Must be between 4 and 32 characters long and start with a letter. You may use letters, numbers, underscores, and one dot");

		home.inputText(home.username, "adeaheoako1owesokmjsd2oi48.ok_dik");
		textoDeErro = home.getElement(home.usernameToastError);
		assertEquals(textoDeErro.getAttribute("innerText"),
				"Must be between 4 and 32 characters long and start with a letter. You may use letters, numbers, underscores, and one dot");
		home.inputText(home.username, "adenos_accenture.com");

		// Valida entrada no campo password pelo menos 6 caracteres, um numero, uma lera
		// maiúscula e uma letra minúscula
		home.inputText(home.password, "An1m4");
		textoDeErro = home.getElement(home.passwordToastError);
		assertEquals(textoDeErro.getAttribute("innerText"),
				"Must be at least 6 characters long, and contain at least one number, one uppercase and one lowercase letter");

		home.inputText(home.password, "123456");
		textoDeErro = home.getElement(home.passwordToastError);
		assertEquals(textoDeErro.getAttribute("innerText"),
				"Must be at least 6 characters long, and contain at least one number, one uppercase and one lowercase letter");

		home.inputText(home.password, "123456R");
		textoDeErro = home.getElement(home.passwordToastError);
		assertEquals(textoDeErro.getAttribute("innerText"),
				"Must be at least 6 characters long, and contain at least one number, one uppercase and one lowercase letter");

		home.inputText(home.password, "123456Rn");

		home.inputText(home.confirmPassword, "123456RDiferente");
		textoDeErro = home.getElement(home.confirmPasswordToastError);
		assertEquals(textoDeErro.getAttribute("innerText"), "Must have the same value as the Password field");

		home.inputText(home.confirmPassword, "123456Rn");

		evidencias.add("preencho o formulario " + pageTitle);
		home.screeshot("preencho o formulario " + pageTitle);

	}

	@E("clico em enviar")
	public void clico_em_enviar() {

		home.clickButton(home.sendMail);

	}

	@Entao("devo receber a mensagem em um modal {string}")
	public void devo_receber_a_mensagem_em_um_modal(String mensagem) throws Exception {

		// Vai aguardar até o modal ficar visível, mas com tempo limite
		home.waitToElement("div.sweet-alert.showSweetAlert.visible");
		assertEquals(mensagem, "Sending e-mail success!");

		evidencias.add("devo receber a mensagem em um modal " + mensagem);
		home.screeshot("devo receber a mensagem em um modal " + mensagem);
		EvidenceFilesGenerator.createEvidenceDoc(titulo, evidencias);

	}

}
