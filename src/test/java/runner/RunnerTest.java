package runner;

import org.junit.runner.RunWith;
import io.cucumber.junit.Cucumber;
import io.cucumber.junit.CucumberOptions;

//TODO Não está rodando pelo runner, verificar glue
@RunWith(Cucumber.class)
@CucumberOptions(plugin = {
		"pretty" }, features = "src/test/resources/features/TricentisForm.feature", glue = "steps")
public class RunnerTest {

}
